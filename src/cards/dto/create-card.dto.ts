import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsUUID } from 'class-validator';

export class CreateCardDto {
  @ApiProperty({ example: 'name', description: 'name', type: String })
  @IsString()
  name: string;

  @ApiProperty({ description: 'columnId', type: String, format: 'uuid' })
  @IsUUID()
  columnId: string;
}
